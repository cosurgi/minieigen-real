#!/bin/bash
# autopkgtest check for minieigen
# (C) 2015 Anton Gladky <gladk@debian.org>
# (C) 2019 Janek Kozicki

set -e

LIBTOTEST=${1}
DEC_DIGITS=${2}

WORKDIR=$(mktemp -d)
trap "rm -rf $WORKDIR" 0 INT QUIT ABRT PIPE TERM
cp ${LIBTOTEST}.so ${WORKDIR}
cd ${WORKDIR}

cat <<EOF > pysmalltest.py
import unittest, math, sys, mpmath
import ${LIBTOTEST} as mne

class SimpleTests(unittest.TestCase):
	def setUp(self):
		self.testLevelsHP        = mne.RealHPConfig.getSupportedByMinieigen()
		self.baseDigits          = mne.RealHPConfig.getDigits10(1) ## ${DEC_DIGITS}
		self.builtinHP           = { 6 : [6,15,18,24,33] , 15 : [15,33] } # higher precisions are multiplies of baseDigits, see NthLevelRealHP in lib/high-precision/RealHP.hpp

	def checkRelativeError(self,a,b):
		self.assertLessEqual(abs( (mpmath.mpf(a)-mpmath.mpf(b))/mpmath.mpf(b) ),self.tolerance)
	def checkRelativeComplexError(self,a,b):
		self.assertLessEqual(abs( (mpmath.mpc(a)-mpmath.mpc(b))/mpmath.mpc(b) ),self.tolerance)
	def getDigitsHP(self,N):
		ret = None
		if (self.baseDigits in self.builtinHP) and (N <= len(self.builtinHP[self.baseDigits])):
			ret = self.builtinHP[self.baseDigits][N-1]
		else:
			ret = self.baseDigits*N
		self.assertEqual(ret,mne.RealHPConfig.getDigits10(N))
		return ret
	def runCheck(self,N,func):
		# tolerance = 1.001×10⁻ᵈ⁺¹, where ᵈ==self.baseDigits
		# so basically we store one more decimal digit, and expect one less decimal digit. That amounts to ignoring one (actually two) least significant digits.
		self.tolerance = (mpmath.mpf(10)**(-self.getDigitsHP(N)+1))*mpmath.mpf("1.001")
		mpmath.mp.dps  = self.getDigitsHP(N) + mne.RealHPConfig.extraStringDigits10
		HPn = ("HP" + str(N)); # the same as the line 'std::string n   = "HP" + boost::lexical_cast<std::string>(N)' in registerRealHP
		if(N==1):
			func(N,mne)      # test global scope functions with RealHP<1>
		func(N,getattr(mne,HPn)) # test scopes HP1, HP2, etc

	def HPtestSimple(self,N,HPn):
		zz=mpmath.acos(0)
		print(zz.__repr__())
		print("zz:",hex(id(zz)))
		print("mpmath:",hex(id(mpmath)))
		a=HPn.Var()
		a.val=zz
		self.assertEqual(mpmath.mp.dps , self.getDigitsHP(N) + mne.RealHPConfig.extraStringDigits10 )
		print("---- a.val=",a.val.__repr__())
		print("---- zz   =",zz   .__repr__())
		print("---- DPS  =",mpmath.mp.dps)
		print("---- abs  =",abs(mpmath.mpf(a.val-zz)))
		print("---- 10** =",self.tolerance)
		self.checkRelativeError(a.val,zz)

	def testSimple(self):
		for N in self.testLevelsHP:
			self.runCheck(N , self.HPtestSimple)

	def HPtestComplex(self,N,HPn):
		print("================== Complex numbers test ==================")
		zz=mpmath.acos(2)
		print(zz.__repr__())
		print("zz:",hex(id(zz)))
		print("mpmath:",hex(id(mpmath)))
		a=HPn.Var()
		a.cpl=zz
		self.assertEqual(mpmath.mp.dps , self.getDigitsHP(N) + mne.RealHPConfig.extraStringDigits10 )
		print("---- a.cpl=",a.cpl.__repr__())
		print("---- abs  =",abs(mpmath.mpc(a.cpl-zz)))
		self.checkRelativeComplexError(a.cpl,zz)
	
	def testComplex(self):
		for N in self.testLevelsHP:
			self.runCheck(N , self.HPtestComplex)

if __name__ == '__main__':
		unittest.main(testRunner=unittest.TextTestRunner(stream=sys.stdout, verbosity=2))
EOF

function handle_error() {
	ls -la
	gdb --batch -ex "bt full" `which python3` ./core
	exit 1
}

echo 'Test Python3'
ulimit -c unlimited
python3 pysmalltest.py || handle_error
echo "Python3 run: OK"

cd
rm -rf ${WORKDIR}

