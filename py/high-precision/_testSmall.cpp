/*************************************************************************
*  2019 Janek Kozicki                                                    *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

#include <boost/core/demangle.hpp>

// How I was checking std::complex:
//    https://cpp.hotexamples.com/examples/-/-/PyFloat_AsDouble/cpp-pyfloat_asdouble-function-examples.html
//    https://docs.python.org/3/c-api/float.html
//    https://bugs.python.org/issue628842
// How to use PyErr_Occurred()
//    https://stackoverflow.com/questions/1796510/accessing-a-python-traceback-from-the-c-api
//    https://docs.python.org/3/c-api/exceptions.html#c.PyErr_Occurred
// minieigen sources
//    apt-get source minieigen
//    /tmp/minieigen/minieigen-0.50.3+dfsg1/src
// How to call mpmath, tzn boost::python::eval
//    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/tutorial/tutorial/embedding.html
//    https://docs.python.org/2/extending/index.html
//    https://www.boost.org/doc/libs/1_47_0/libs/python/doc/v2/import.html
//    https://stackoverflow.com/questions/937884/how-do-i-import-modules-in-boostpython-embedded-python-code
// !→ https://misspent.wordpress.com/2009/09/27/how-to-write-boost-python-converters/
//    https://docs.python.org/2/extending/embedding.html
// How I was grepping python sources:
//    /home/deb/deb/python/python3.7-3.7.3  z apt-get source (ctags)
// !→ /home/deb/deb/python/pybind11-2.2.4/include/pybind11  też z apt-get source (ctags)
//    https://packages.debian.org/search?suite=sid&searchon=names&keywords=pybind
//    https://github.com/pybind/pybind11
// Interesting mppp library:
//    https://github.com/bluescarni/mppp
//    https://bluescarni.github.io/mppp/
//    git clone https://github.com/bluescarni/mppp.git
//    /home/deb/deb/python/mppp/mppp   (ctags)
// !→ /home/deb/deb/python/mppp/mppp/include/mp++/extra/pybind11.hpp
// More links:
//    http://mpmath.org/    →     https://github.com/fredrik-johansson/mpmath/blob/master/mpmath/ctx_mp_python.py
//    http://flintlib.org/ który używa Arb i z niego korzysta mppp
//    https://packages.debian.org/search?searchon=sourcenames&keywords=flint
//    http://fredrikj.net/math/x2019.pdf  Arb
// !→ https://pyformat.info/
// Boost python:
//    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/tutorial/index.html
//    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/article.html
//    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/index.html
//    https://www.boost.org/doc/libs/1_42_0/libs/python/doc/v2/faq.html#topythonconversionfailed
//    https://docs.python.org/2.5/ext/dnt-basics.html
//    https://wiki.python.org/moin/boost.python/HowTo

#include <lib/high-precision/Real.hpp>
#include <lib/high-precision/RealHPConfig.hpp>

#include <lib/base/AliasCGAL.hpp>
#include <boost/python.hpp>
#include <iostream>
#include <limits>
#include <sstream>

//#define ARBITRARY_REAL_DEBUG
#include <lib/high-precision/ToFromPythonConverter.hpp>

namespace py = ::boost::python;
using ::yade::Complex;
using ::yade::ComplexHP;
using ::yade::Real;
using ::yade::RealHP;

template <int N>
struct Var {
	static int stat;

	RealHP<N>    value { -71.23 };
	ComplexHP<N> valueComplex { -71.23, 33.23 };

	RealHP<N> get() const { return value; };
	void      set(RealHP<N> val) { value = val; };

	ComplexHP<N> getComplex() const { return valueComplex; };
	void         setComplex(ComplexHP<N> val) { valueComplex = val; };
};

//https://stackoverflow.com/questions/3229883/static-member-initialization-in-a-class-template
template <int N> int Var<N>::stat = -100-N;

#include <boost/python/def.hpp>
int f(double x, double y, double z = 0.0, double w = 1.0)
{
	std::cerr << "x=" << x << "\n";
	std::cerr << "y=" << y << "\n";
	std::cerr << "z=" << z << "\n";
	std::cerr << "w=" << w << "\n";
	return 92;
}

double someFunction()
{
	double a   = 10.11;
	double b   = 30.11;
	double y   = a * b + b * a + b - a;
	double ret = std::pow(y, a);
	std::cerr << ret << "\n";
	return ret;
}

namespace yade {
Real Oriented_squared_distance2(Plane P, CGALpoint x)
{
	Real h = P.a() * x.x() + P.b() * x.y() + P.c() * x.z() + P.d();
	return ((h > 0.) - (h < 0.)) * pow(h, 2) / (CGALvector(P.a(), P.b(), P.c())).squared_length();
}
void testCgalNumTraits()
{
	CGALpoint  a(Real(2), Real(2), Real(2));
	CGALpoint  p1(Real(0), Real(0), Real(0));
	CGALvector v1(Real(1), Real(1), Real(1));
	Plane      p(p1, v1);
	auto       b = a[0] + 1;
#ifdef YADE_REAL_MPFR_NO_BOOST_experiments_only_never_use_this
	auto digs1 = std::numeric_limits<Real>::digits10() + 1;
#else
	auto digs1 = std::numeric_limits<Real>::digits10 + 1;
#endif
	std::cout << "\n\n" << std::setprecision(digs1);
	std::cout << "++++==== testCGAL, b = " << b << "\n";
	std::cout << "++++==== b demangle  = " << boost::core::demangle(typeid(b).name()) << "\n";
	std::cout << "++++==== sq dist     = " << Oriented_squared_distance2(p, a) << "\n";
	std::cout << "++++==== Sqrt        = " << CGAL::sqrt(a[0]) << "\n";
	std::cout << "++++==== Kth_root    = " << CGAL::kth_root(3, a[0]) << "\n";
	auto interval = CGAL::to_interval(a[0]);
	std::cout << "++++==== To_interval = " << interval.first << "  " << interval.second << "\n";
	std::cout << "++++==== To_double   = " << boost::core::demangle(typeid(CGAL::to_double(a[0])).name()) << " " << CGAL::to_double(a[0]) << "\n";
	//	std::cout << "++++==== Is_valid    = " << Is_valid(a[0])   << "\n";
	//	std::cout << "++++==== Is_finite   = " << Is_finite(a[0])   << "\n";

	std::vector<CGALpoint> pointsCGAL;
	pointsCGAL.push_back(CGALpoint(0, 0, 0));
	pointsCGAL.push_back(CGALpoint(1, 0, 0));
	pointsCGAL.push_back(CGALpoint(0, 1, 0));
	pointsCGAL.push_back(CGALpoint(0, 0, 1));
	Polyhedron P;
	CGAL::convex_hull_3(pointsCGAL.begin(), pointsCGAL.end(), P);

	Plane                fit;
	std::vector<Segment> segments;
	segments.push_back(Segment(pointsCGAL[0], pointsCGAL[1]));
	segments.push_back(Segment(pointsCGAL[1], pointsCGAL[2]));
	segments.push_back(Segment(pointsCGAL[2], pointsCGAL[3]));
	linear_least_squares_fitting_3(segments.begin(), segments.end(), fit, CGAL::Dimension_tag<1>());

	Triangle t(pointsCGAL[0], pointsCGAL[1], pointsCGAL[2]);
	std::cout << "++++==== do_intersect= " << CGAL::do_intersect(t, segments[0]) << "\n";

	std::cout << "\n\n";
}
}

// ALL DOCS         : https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/index.html
//              note: clicking 'next →' skips tutorial and references.
//                    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/tutorial
//                    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/index.html
//              good: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/tutorial/tutorial/techniques.html
//                    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/high_level_components.html
//         operators: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/high_level_components/boost_python_operators_hpp.html
//              enum: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/tutorial/tutorial/object.html#tutorial.object.enums
//                    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/high_level_components/boost_python_enum_hpp.html
//             scope: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/high_level_components/boost_python_scope_hpp.html
// python object ref: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/object_wrappers/boost_python_object_hpp.html
//               arg: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/function_invocation_and_creation.html
// arbitrary arg num: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/function_invocation_and_creation/boost_python_raw_function_hpp.html
//  call python func: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/topics.html
//                    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/function_invocation_and_creation/boost_python_call_hpp.html
//                    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/function_invocation_and_creation/boost_python_call_method_hpp.html
// virtual functions: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/topics.html#topics.calling_python_functions_and_met.introduction
//               ptr: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/function_invocation_and_creation/boost_python_ptr_hpp.html
//                    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/to_from_python_type_conversion/boost_python_register_ptr_to_pyt.html
//                    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/utility_and_infrastructure/boost_python_pointee_hpp.html
//  handle, borrowed: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/utility_and_infrastructure/boost_python_handle_hpp.html
//            pickle: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/topics/pickle_support.html
//                    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/topics/pickle_support.html#topics.pickle_support.light_weight_alternative_pickle_
//   indexing vector: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/topics/indexing_support.html
//       boost numpy: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/numpy/index.html
// docstring_options: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/function_invocation_and_creation/function_documentation.html
//                    https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/high_level_components/boost_python_docstring_options_h.html

#include <boost/core/demangle.hpp>
template <typename A, int Level = ::yade::math::levelOfRealHP<A>> void tstMFunc(A arg)
{
	::yade::ComplexHP<Level> cpl(arg,2*arg);
	std::cout << std::setprecision(std::numeric_limits<A>::digits10);
	std::cout << arg << " *** Level == " << Level 
		<< " ceil:" << ::yade::math::ceil(arg)
		<< " sin:" << ::yade::math::sin(arg)
		<< " sin:" << ::yade::math::sin(cpl)
		<< " abs:" << ::yade::math::abs(arg)
		<< " abs:" << ::yade::math::abs(cpl)
		<< " asin:" << ::yade::math::asin(arg)
// FIXED : test all Complex functions pow, asin, sqrt itp.
// FIXED : two argument: SelectHigherHP<Rr,T>; a variant of SelectHigherHP which works gdy arg is int or sth else.
// FIXME : old note: pow() return type: SelectHigherHP<Rr,T> ? Add ComplexHP<…> support, even if only one arg is complex
//         the levelOfRealHP< > in pow() should take level of boh arguments? Or the one which is higher?
//		<< " asin:" << ::yade::math::asin(cpl)
		<< " lvl:" << ::yade::math::levelOfHP<A>
		<< " lvl complex:" << ::yade::math::levelOfHP<decltype(cpl)>
	//	<< " cos:" << ::yade::math::cos(arg)
		<<  "\n";
}
template <typename A> int  deduceNumber() { return ::yade::math::levelOrZero<A>; }
template <typename A> void prn()
{
	std::cout << "digits10: " << std::numeric_limits<A>::digits10 << "\n";
	std::cout << "digits  : " << std::numeric_limits<A>::digits << "\n";
	tstMFunc(A(0.2345678901234567890));
	std::cout << "HP multiplicity level: " << deduceNumber<A>() << "\n";
	std::cout << boost::core::demangle(typeid(A).name()) << "\n\n";
}

#if __cplusplus >= 201700L
// in C++17 we can use inline variables and fold-expressions
template <int... T>
constexpr inline auto for_each_int = [](auto&& f) {
    (f.template operator()<T>(), ...);
};
#else
// in C++14 we have to use boost::mpl
#include <boost/mpl/range_c.hpp>
#include <boost/mpl/for_each.hpp>
#endif
#include <boost/concept/assert.hpp>
#include <boost/math/concepts/real_type_concept.hpp>

#include <lib/high-precision/UpconversionOfBasicOperatorsHP.hpp>
// XXX : duplicate
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/range_c.hpp>

namespace yade {
template <int N1, int N2>
void test_types_N1_N2() {
	BOOST_CONCEPT_ASSERT((boost::math::concepts::RealTypeConcept<RealHP<N1>>));

	if(::yade::math::isHP<RealHP   <N1>> == false) { std::cerr << "Fatal is1\n"; exit(1); };
	if(::yade::math::isHP<RealHP   <N2>> == false) { std::cerr << "Fatal is2\n"; exit(1); };
	if(::yade::math::isHP<ComplexHP<N1>> == false) { std::cerr << "Fatal is3\n"; exit(1); };
	if(::yade::math::isHP<ComplexHP<N2>> == false) { std::cerr << "Fatal is4\n"; exit(1); };
	std::cout << "testing RealHP<" << N1 << ">\t  and RealHP<" << N2 << "> ";
	{
		RealHP<N1> a= static_cast<RealHP<N1>>(-1.25 );
		RealHP<N2> b= static_cast<RealHP<N2>>( 2.5  );
		auto c1 = a+b;
		auto c2 = a-b;
		auto c3 = a*b;
		auto c4 = a/b;
		//tstMFunc(c1);
		//tstMFunc(c2);
		//tstMFunc(c3);
		//tstMFunc(c4);
		if(c1 != RealHP<std::max(N1,N2)>( 1.25 )) {std::cerr << "Fatal r1\n"; exit(1);};
		if(c2 != RealHP<std::max(N1,N2)>(-3.75 )) {std::cerr << "Fatal r2\n"; exit(1);};
		if(c3 != RealHP<std::max(N1,N2)>(-3.125)) {std::cerr << "Fatal r3\n"; exit(1);};
		if(c4 != RealHP<std::max(N1,N2)>(-0.5  )) {std::cerr << "Fatal r4\n"; exit(1);};
		auto d1 = a;
		auto d2 = b;
		RealHP<N2> d3 = RealHP<N2>(a);
		RealHP<N1> d4 = RealHP<N1>(b);
		auto d5 = d1+d2+d3+d4;
		if(d5 != RealHP<std::max(N1,N2)>( 2.5  )) {std::cerr << "Fatal r5\n"; exit(1);};
	}
	{
		std::cout << " \t testing ComplexHP<" << N1 << ">\t  and ComplexHP<" << N2 << "> ";
		ComplexHP<N1> a= ComplexHP<N1>(-1.25 , 0.5 );
		ComplexHP<N2> b= ComplexHP<N2>( 1.0  , 1.0 );
		auto c1 = a+b;
		auto c2 = a-b;
		auto c3 = a*b;
		auto c4 = a/b;
		//std::cout << std::setprecision(std::numeric_limits<RealHP<std::max(N1,N2)>>::digits10 + 1);
		//std::cout << c4 << " ----\n";
		//std::cout << ComplexHP<std::max(N1,N2)>(-0.375 , 0.875 ) << " ----\n";
		if(c1 != ComplexHP<std::max(N1,N2)>(-0.25  , 1.5   )) {std::cerr << "Fatal c1\n"; exit(1);};
		if(c2 != ComplexHP<std::max(N1,N2)>(-2.25  ,-0.5   )) {std::cerr << "Fatal c2\n"; exit(1);};
		if(c3 != ComplexHP<std::max(N1,N2)>(-1.75  ,-0.75  )) {std::cerr << "Fatal c3\n"; exit(1);};
		if(c4 != ComplexHP<std::max(N1,N2)>(-0.375 , 0.875 )) {std::cerr << "Fatal c4\n"; exit(1);};
		auto d1 = a;
		auto d2 = b;
		// down-converting requires extra casting
		ComplexHP<N2> d3 = ComplexHP<N2>(RealHP<N2>(a.real()),RealHP<N2>(a.imag()));
		ComplexHP<N1> d4 = ComplexHP<N1>(RealHP<N1>(b.real()),RealHP<N1>(b.imag()));
		auto d5 = d1+d2+d3+d4;
		if(d5 != ComplexHP<std::max(N1,N2)>(-0.5   , 3.0   )) {std::cerr << "Fatal c5\n"; exit(1);};
	}
	{
		std::cout << " \t testing ComplexHP<" << N1 << ">\t  and RealHP<" << N2 << "> ";
		ComplexHP<N1> a = ComplexHP<N1>(-1.25 , 0.5 );
		RealHP<N2>    b = RealHP<N2>   ( 1.0 );
		auto c1 = a+b;
		auto c2 = a-b;
		auto c3 = a*b;
		auto c4 = a/b;
		auto c5 = b+a;
		auto c6 = b-a;
		auto c7 = b*a;
		auto c8 = b/a;
		//std::cout << std::setprecision(std::numeric_limits<RealHP<std::max(N1,N2)>>::digits10 + 1);
		//std::cout << c4 << " ----\n";
		//std::cout << ComplexHP<std::max(N1,N2)>(-0.375 , 0.875 ) << " ----\n";
		if(c1 != ComplexHP<std::max(N1,N2)>(-0.25  , 0.5   )) {std::cerr << "Fatal cr1\n"; exit(1);};
		if(c2 != ComplexHP<std::max(N1,N2)>(-2.25  , 0.5   )) {std::cerr << "Fatal cr2\n"; exit(1);};
		if(c3 != ComplexHP<std::max(N1,N2)>(-1.25  , 0.5   )) {std::cerr << "Fatal cr3\n"; exit(1);};
		if(c4 != ComplexHP<std::max(N1,N2)>(-1.25  , 0.5   )) {std::cerr << "Fatal cr4\n"; exit(1);};
		if(c5 != ComplexHP<std::max(N1,N2)>(-0.25  , 0.5   )) {std::cerr << "Fatal cr5\n"; exit(1);};
		if(c6 != ComplexHP<std::max(N1,N2)>( 2.25  ,-0.5   )) {std::cerr << "Fatal cr6\n"; exit(1);};
		if(c7 != ComplexHP<std::max(N1,N2)>(-1.25  , 0.5   )) {std::cerr << "Fatal cr7\n"; exit(1);};
		if(::yade::math::abs(c8 - ComplexHP<std::max(N1,N2)>(-0.68965517241379310345  , -0.27586206896551724138 )) > 0.1) {std::cerr << "Fatal cr8\n"; exit(1);};
		auto d1 = a;
		auto d2 = b;
		// down-converting requires extra casting
		ComplexHP<N2> d3 = ComplexHP<N2>(RealHP<N2>(a.real()),RealHP<N2>(a.imag()));
	//XXX	ComplexHP<N2> d3 = ComplexHP<N2>(a);
		RealHP   <N1> d4 = RealHP   <N1>(b);
		auto d5 = d1+d2+d4+d3;
		if(d5 != ComplexHP<std::max(N1,N2)>(-0.5   , 1.0   )) {std::cerr << "Fatal cr5\n"; exit(1);};
		auto d6 = d1+d2+d3+d4;
		if(d6 != ComplexHP<std::max(N1,N2)>(-0.5   , 1.0   )) {std::cerr << "Fatal cr6\n"; exit(1);};

		static_assert(std::is_same<ComplexHP<std::max(N1,N2)>,decltype(c1)>::value, "Assert error c1");
		static_assert(std::is_same<ComplexHP<std::max(N1,N2)>,decltype(c2)>::value, "Assert error c2");
		static_assert(std::is_same<ComplexHP<std::max(N1,N2)>,decltype(c3)>::value, "Assert error c3");
		static_assert(std::is_same<ComplexHP<std::max(N1,N2)>,decltype(c4)>::value, "Assert error c4");
		static_assert(std::is_same<ComplexHP<std::max(N1,N2)>,decltype(c5)>::value, "Assert error c5");
		static_assert(std::is_same<ComplexHP<std::max(N1,N2)>,decltype(c6)>::value, "Assert error c6");
		static_assert(std::is_same<ComplexHP<std::max(N1,N2)>,decltype(c7)>::value, "Assert error c7");
		static_assert(std::is_same<ComplexHP<std::max(N1,N2)>,decltype(c8)>::value, "Assert error c8");
		static_assert(std::is_same<ComplexHP<         N1    >,decltype(d1)>::value, "Assert error d1");
		static_assert(std::is_same<    decltype(b)           ,decltype(d2)>::value, "Assert error d2");
		static_assert(std::is_same<ComplexHP<            N2 >,decltype(d3)>::value, "Assert error d3");
		static_assert(std::is_same<RealHP<            N1    >,decltype(d4)>::value, "Assert error d4");
		static_assert(std::is_same<ComplexHP<std::max(N1,N2)>,decltype(d5)>::value, "Assert error d5");
		static_assert(std::is_same<ComplexHP<std::max(N1,N2)>,decltype(d6)>::value, "Assert error d6");
	}
	std::cout << "\n";
};
}

template <int N, bool registerConverters> struct RegisterRealHPSmall {
	static void work(const py::scope& topScope, const py::scope& scopeHP)
	{
		if (registerConverters) {
			py::scope top(topScope);
			ArbitraryComplex_from_python<ComplexHP<N>>();
			py::to_python_converter<ComplexHP<N>, ArbitraryComplex_to_python<ComplexHP<N>>>();

			ArbitraryReal_from_python<RealHP<N>>();
			py::to_python_converter<RealHP<N>, ArbitraryReal_to_python<RealHP<N>>>();
		}

		py::scope HPn(scopeHP);

		py::class_<Var<N>>("Var")
		        .add_property("val", &Var<N>::get, &Var<N>::set, "test docstring1")
		        .add_property("cpl", &Var<N>::getComplex, &Var<N>::setComplex, "test docstring2")
		        .add_static_property(
		                "stat",
		                py::make_getter(&Var<N>::stat, py::return_value_policy<py::return_by_value>()),
		                py::make_setter(&Var<N>::stat, py::return_value_policy<py::return_by_value>()) /*, "test docstring3"*/);
	}
};

BOOST_PYTHON_MODULE(THE_CPP_NAME)
try {
	using ::yade::math::RealHP;
	using ::yade::math::levelOfRealHP;
	using ::yade::math::isHP;
	using ::yade::math::detail::NthLevel;
// XXX: Each of them should generate compilation errors: XXX
// XXX: --- XXX --- check them periodically --- XXX ---: XXX
//	tstMFunc("asd");
//	tstMFunc(std::string("asf"));
//	prn<RealHP<-5.5>>();
//	prn<RealHP<-55>>();
//	prn<RealHP<-1>>();
//	prn<RealHP<0>>();
//	tstMFunc(1.0f); // this one fails for anything higher than float.

	// template loop that tests arithmetic operations between different RealHP<n> types.
#if __cplusplus >= 201700L
// in C++17 we can use inline variables and fold-expressions
	auto for_each_test_N = for_each_int<1,2,3,4,5,6,7,8,9,10>;
	for_each_test_N([&]<int N1>() {
		for_each_test_N([]<int N2>() {
			yade::test_types_N1_N2<N1,N2>();
		});
	});
#else
// in C++14 we have to use boost::mpl
	boost::mpl::for_each< boost::mpl::range_c<int,1,11> >( []<typename N1>(N1) {
		boost::mpl::for_each< boost::mpl::range_c<int,1,11> >([]<typename N2>(N2) {
			yade::test_types_N1_N2<N1::value,N2::value>();
		});
	});
#endif

// Was testing pow here.
//	RealHP<1> dispDiameter = 44.44;
//	RealHP<1> gridStep = pow(10, (floor(0.5 + log10(dispDiameter))));
//	std::cout << gridStep << "\n";

	prn<RealHP<1>>();
	prn<RealHP<2>>();
	prn<RealHP<3>>();
	prn<RealHP<4>>();
	prn<RealHP<5>>();
	prn<RealHP<6>>();
	prn<RealHP<10>>();
	// too slow.
	//prn<RealHP<55>>();
	//tstMFunc(RealHP<111>(1.2345678901234567890));

// FIXED - conversion problems.
//         addition          float128 + MPFR
//         construction of   MPFR(ThinRealWrapper)
	{
		using namespace yade; // UpconversionOfBasicOperatorsHP
		RealHP<1>  a = -1.0;
		RealHP<15> b(a);
		auto       c = a + b;
		//auto c=RealHP<15>(a)+b; // without UpconversionOfBasicOperatorsHP this line has to be used.
		tstMFunc(c);
	}

	std::cout << " ?? " << std::numeric_limits<const char*>::digits10 << "\n";
	std::cout << " ?? " << std::numeric_limits<std::string>::digits10 << "\n";
	
	std::cout << std::boolalpha; // print true/false
	std::cout << "Lev= " << levelOfRealHP<RealHP<1> > << "\tis same: " << isHP<RealHP<1>  > << " \t" << NthLevel<RealHP<1>  > << " \t" << std::numeric_limits<RealHP<1>  >::digits10 << "\n";
	std::cout << "Lev= " << levelOfRealHP<RealHP<2> > << "\tis same: " << isHP<RealHP<2>  > << " \t" << NthLevel<RealHP<2>  > << " \t" << std::numeric_limits<RealHP<2>  >::digits10 << "\n";
	std::cout << "Lev= " << levelOfRealHP<RealHP<3> > << "\tis same: " << isHP<RealHP<3>  > << " \t" << NthLevel<RealHP<3>  > << " \t" << std::numeric_limits<RealHP<3>  >::digits10 << "\n";
	std::cout << "Lev= " << levelOfRealHP<RealHP<4> > << "\tis same: " << isHP<RealHP<4>  > << " \t" << NthLevel<RealHP<4>  > << " \t" << std::numeric_limits<RealHP<4>  >::digits10 << "\n";
	std::cout << "Lev= " << levelOfRealHP<RealHP<5> > << "\tis same: " << isHP<RealHP<5>  > << " \t" << NthLevel<RealHP<5>  > << " \t" << std::numeric_limits<RealHP<5>  >::digits10 << "\n";
	std::cout << "Lev= " << levelOfRealHP<RealHP<6> > << "\tis same: " << isHP<RealHP<6>  > << " \t" << NthLevel<RealHP<6>  > << " \t" << std::numeric_limits<RealHP<6>  >::digits10 << "\n";
	std::cout << "Lev= " << levelOfRealHP<RealHP<10>> << "\tis same: " << isHP<RealHP<10> > << " \t" << NthLevel<RealHP<10> > << " \t" << std::numeric_limits<RealHP<10> >::digits10 << "\n";
	std::cout << "Lev= " << levelOfRealHP<RealHP<55>> << "\tis same: " << isHP<RealHP<55> > << " \t" << NthLevel<RealHP<55> > << " \t" << std::numeric_limits<RealHP<55> >::digits10 << "\n";
	std::cout << "Lev= ?"                             << "\tis same: " << isHP<std::string> << " \t" << NthLevel<std::string> << " \t" << std::numeric_limits<std::string>::digits10 << "\n";
	std::cout << "Lev= ?"                             << "\tis same: " << isHP<const char*> << " \t" << NthLevel<const char*> << " \t" << std::numeric_limits<const char*>::digits10 << "\n";
	std::cout << "Lev= ?"                             << "\tis same: " << isHP<char[2]    > << " \t" << NthLevel<char[2]    > << " \t" << /*std::numeric_limits<char[2]    >::digits10 <<*/ "\n";

	// what about BOOST_PYTHON_PY_SIGNATURES_PROPER_INIT_SELF_TYPE
	// If defined the python type of __init__ method "self" parameters is properly generated, otherwise object is used.
	// https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/configuration.html

	// If it can't find a propeer conversion:
	// https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/faq/why_is_my_automatic_to_python_co.html

	::yade::math::detail::registerLoopForHPn<::yade::math::RealHPConfig::SupportedByEigenCgal, RegisterRealHPSmall>();

#ifdef YADE_REAL_MPFR_NO_BOOST_experiments_only_never_use_this
	mpfr::mpreal::set_default_prec(YADE_REAL_BIT + ::yade::math::RealHPConfig::extraStringDigits10);
#endif

	Real startPrecision = std::numeric_limits<Real>::epsilon(); //Eigen::NumTraits<Scalar>::dummy_precision();
	std::cerr << __FILE__ << ":" << __LINE__ << " startPrecision: " << startPrecision << "\n";
	boost::python::object startPrecisionPyObj { startPrecision };

	::yade::math::RealHPConfig::pyRegister();

	py::def("f", f, (py::arg("x"), "y", py::arg("z") = 0.0, py::arg("w") = someFunction()));

	::yade::Matrix3cr m, n;
	m << Complex(1), Complex(2), Complex(3), Complex(4), Complex(5), Complex(6), Complex(7), Complex(8), Complex(9);
	std::cout << m << "\n";
	n = static_cast<Complex>(2) * m;
	//n = 2*m; // XXX: does no work. Explicit conversion is necessary.
	std::cout << n << "\n";
	n = m * Complex(2);
	std::cout << n << "\n";

	::yade::testCgalNumTraits();

	/*} catch(const py::error_already_set& e) {
	PyErr_Print();
//	boost::python::handle_exception();
throw e;
//	PyErr_Clear();
}
catch (...) {
	PyObject *type, *value, *traceback;
	PyErr_Fetch(&type, &value, &traceback);
	throw std::runtime_error(boost::python::call_method<std::string>(value, "__str__"));
	//PyErr_Print();
//	PyErr_Restore(type, value, traceback);
	//PyErr_SetImportError( boost::python::incref(boost::python::object(__FILE__).ptr()) , nullptr, nullptr);
	//boost::python::throw_error_already_set();
//	throw boost::python::error_already_set();
//	boost::python::handle_exception();
//	std::cerr << "Unknown exception\n";
//	throw;
}
*/
} catch (...) {
	// see: https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/high_level_components/boost_python_errors_hpp.html#high_level_components.boost_python_errors_hpp.example
	//      https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/tutorial/tutorial/embedding.html
	//      https://stackoverflow.com/questions/1418015/how-to-get-python-exception-text
	// If we wanted custom yade exceptions thrown to python:
	//     https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/tutorial/tutorial/exception.html
	//     https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/reference/high_level_components/boost_python_exception_translato.html
	std::cerr << ("Importing this module caused an unrecognized exception caught on C++ side and this module is in an inconsistent state now.\n\n");
	PyErr_Print();
	PyErr_SetString(PyExc_SystemError, __FILE__); // raising anything other than SystemError is not possible
	boost::python::handle_exception();
	throw;
}
