This project is used to test the requirements for [YADE](https://yade-dem.org/doc/) [(yade on gitlab)](https://gitlab.com/yade-dev/trunk) to perform calculations with high precision.

This involves testing:

* Eigen and minieigen
* CGAL
* boost multiprecision
* boost python integration

Currently supported types are:

* `long double`
* `boost float128`
* `boost mpfr`
* `boost cpp_bin_float`

And `quad double` with 62 decimal places (package libqd-dev) is in the works with [boost developers](https://github.com/boostorg/multiprecision/issues/184).
